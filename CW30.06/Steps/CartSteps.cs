﻿using System;
using TechTalk.SpecFlow;

namespace CW30._06.Steps
{
    [Binding]
    public class CartSteps
    {
        [Given(@"Allo website is open")]
        public void GivenAlloWebsiteIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User is not Logged in")]
        public void GivenUserIsNotLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User clicks on card")]
        public void WhenUserClicksOnCard()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"Card is empty")]
        public void ThenCardIsEmpty()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
