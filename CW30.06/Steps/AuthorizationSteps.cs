﻿using System;
using TechTalk.SpecFlow;

namespace CW30._06.FeatureFiles
{
    [Binding]
    public class AuthorizationSteps
    {
        [Given(@"Allo wensite is open")]
        public void GivenAlloWensiteIsOpen()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Given(@"User have account")]
        public void GivenUserHaveAccount()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User click Login button")]
        public void WhenUserClickLoginButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User fill emailfield with ""(.*)""")]
        public void WhenUserFillEmailfieldWith(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User fill password field with ""(.*)""")]
        public void WhenUserFillPasswordFieldWith(string p0)
        {
            ScenarioContext.Current.Pending();
        }
        
        [When(@"User click send form button")]
        public void WhenUserClickSendFormButton()
        {
            ScenarioContext.Current.Pending();
        }
        
        [Then(@"User logged in")]
        public void ThenUserLoggedIn()
        {
            ScenarioContext.Current.Pending();
        }
    }
}
